#ifndef HELPERS_HPP
#define HELPERS_HPP

#include <string>
#include <vector> 
#include <iostream> // for std::cout
#include <fstream> // for std::ifstream (loading files from disk)
#include <algorithm> // for std::atof

namespace helpers
{

std::vector< std::string > getLines( const char *filePath )
{
    std::ifstream f( filePath );
    std::vector< std::string > lines;

    if( f )
    {
        std::string line;
        while( std::getline( f , line ))
            lines.push_back( line );
    }
    else std::cout << "Failed to open file:" << filePath << std::endl;

    return lines;
}

}

#endif // HELPERS_HPP
