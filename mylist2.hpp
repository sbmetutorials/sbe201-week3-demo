#ifndef MY_LIST_HPP
#define MY_LIST_HPP

#include <cstdlib>

namespace list
{

template <typename T>
struct Node
{
    T data;
    Node *next;
};

template <typename T>
struct LL
{
    Node<T> *head = nullptr;

    void insertFront( T data)
    {
        auto newNode = new Node<T>{data, head};
        head = newNode;
    }

    void removeFront( )
    {
        auto discard = head;
        head = head->next;
        delete discard;
    }

    T front( )
    {
        if (head)
            return head->data;
        else
            exit(1);
    }

    bool isEmpty()
    {
        return head == nullptr;
    }

    int size()
    {
        int counter = 0;
        auto current = head;
        while (current != nullptr)
        {
            counter++;
            current = current->next;
        }
        return counter;
    }
};
}

#endif