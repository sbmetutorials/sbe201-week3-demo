#include "helpers.hpp"

#include <iostream>
#include <stack>
#include <vector>
#include <list>

char complementaryBase( char base )
{
    if( base == 'A' ) return 'T';
    else if( base == 'T' ) return 'A';
    else if( base == 'G' ) return 'C';
    else return 'G';
}

int main( int argc , char **argv )
{
    std::string dna = helpers::getLines( argv[1] )[0];

    std::stack< char > stack_dna;

    std::string cdna;


    for( char base : dna )
        stack_dna.push( complementaryBase( base ));
    
    while( ! stack_dna.empty() )
    {
        cdna.push_back(  stack_dna.top());
        stack_dna.pop();
    }

    std::cout << cdna;
}