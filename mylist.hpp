#ifndef MY_LIST_HPP
#define MY_LIST_HPP

#include <cstdlib>

namespace list
{

template< typename T >
struct Node
{
    T data;
    Node *next;
};

template< typename T >
struct LL
{
    Node< T > *head = nullptr;
};

template< typename T >
void insertFront( LL< T > &lst ,  T data )
{
    auto newNode = new Node< T >{ data , lst.head };
    lst.head = newNode;
}

template< typename T >
void removeFront( LL< T > &lst )
{
    auto discard = lst.head;
    lst.head = lst.head->next;
    delete discard;
}

template< typename T >
T front( LL< T > &lst )
{
    if( lst.head )
        return lst.head->data;
    else exit( 1 );
}

template< typename T >
bool isEmpty( LL< T > &lst )
{
    return lst.head == nullptr;
}


template< typename T >
int size( LL< T > &lst )
{
    int counter = 0 ;
    auto current = lst.head;
    while( current != nullptr )
    {
        counter++;
        current = current->next;
    }
    return counter;
}

}


#endif