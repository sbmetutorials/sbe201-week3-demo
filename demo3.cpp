#include "mylist.hpp"
#include "helpers.hpp"
#include <iostream>
#include <string>

char complementaryBase( char base )
{
    if( base == 'A' ) return 'T';
    else if( base == 'T' ) return 'A';
    else if( base == 'G' ) return 'C';
    else return 'G';
}

int main( int argc , char **argv )
{
    std::string dna = helpers::getLines( argv[1] )[0];

    list::LL< char > ls;

    std::string cdna;

    for( char base : dna )
    {
        list::insertFront( ls , complementaryBase( base ));
    }

    while( ! list::isEmpty( ls ))
    {
        cdna.push_back( list::front( ls ));
        list::removeFront( ls );
    }

    std::cout << cdna;
}