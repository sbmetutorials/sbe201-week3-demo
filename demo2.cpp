#include <iostream>

/**
 * This demo is made to test valgrind for debugging.
 * **/

struct IntegerNode
{
    int data;
    IntegerNode *next;
};

int main()
{
    IntegerNode *p1 = nullptr;  

    p1->data = 9;

    return 0;
}